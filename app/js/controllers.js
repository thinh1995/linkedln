var linkedlnApp = angular.module('linkedlnApp', ["xeditable"]);

linkedlnApp.controller('ProfileCtrl', function ($scope, $http){
	$http.get('profiles/jeffweiner.json').success(function(data) {
		$scope.jeffweiner = data;
	})
});

linkedlnApp.run(function(editableOptions) {
  editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});

linkedlnApp.directive('readMore', function() {
  return {
    restrict: 'A',
    transclude: true,
    replace: true,
    template: '<p></p>',
    scope: {
      moreText: '@',
      lessText: '@',
      words: '@',
      ellipsis: '@',
      char: '@',
      limit: '@',
      content: '@'
    },
    link: function(scope, elem, attr, ctrl, transclude) {
      var moreText = angular.isUndefined(scope.moreText) ? ' <a class="read-more">Read More...</a>' : ' <a class="read-more">' + scope.moreText + '</a>',
        lessText = angular.isUndefined(scope.lessText) ? ' <a class="read-less">Less ^</a>' : ' <a class="read-less">' + scope.lessText + '</a>',
        ellipsis = angular.isUndefined(scope.ellipsis) ? '' : scope.ellipsis,
        limit = angular.isUndefined(scope.limit) ? 150 : scope.limit;

      attr.$observe('content', function(str) {
        readmore(str);
      });

      transclude(scope.$parent, function(clone, scope) {
        readmore(clone.text().trim());
      });

      function readmore(text) {

        var text = text,
          orig = text,
          regex = /\s+/gi,
          charCount = text.length,
          wordCount = text.trim().replace(regex, ' ').split(' ').length,
          countBy = 'char',
          count = charCount,
          foundWords = [],
          markup = text,
          more = '';

        if (!angular.isUndefined(attr.words)) {
          countBy = 'words';
          count = wordCount;
        }

        if (countBy === 'words') { // Count words

          foundWords = text.split(/\s+/);

          if (foundWords.length > limit) {
            text = foundWords.slice(0, limit).join(' ') + ellipsis;
            more = foundWords.slice(limit, count).join(' ');
            markup = text + moreText + '<span class="more-text">' + more + lessText + '</span>';
          }

        } else { // Count characters

          if (count > limit) {
            text = orig.slice(0, limit) + ellipsis;
            more = orig.slice(limit, count);
            markup = text + moreText + '<span class="more-text">' + more + lessText + '</span>';
          }

        }

        elem.append(markup);
        elem.find('.read-more').on('click', function() {
          $(this).hide();
          elem.find('.more-text').addClass('show').slideDown();
        });
        elem.find('.read-less').on('click', function() {
          elem.find('.read-more').show();
          elem.find('.more-text').hide().removeClass('show');
        });

      }
    }
  };
});

linkedlnApp.controller('EditableTableCtrl', function($scope, $filter, $http, $q) {
  $scope.checkName = function(data) {
    if (data ==='') {
      return "Can not be blank";
    }
  };

  // filter rows to show
  $scope.filterRow = function(row) {
    return row.isDeleted !== true;
  };

  // mark row as deleted
  $scope.deleteRow = function(id) {
    var filtered = $filter('filter')($scope.jeffweiner.overview, {'id': id});
    if (filtered.length) {
      filtered[0].isDeleted = true;
    }
  };

  // add row
  $scope.addRow = function() {
   $scope.jeffweiner.overview.push({
      'id': $scope.jeffweiner.overview.length+1,
      'type': '',
      'content': '',
      isNew: true
    });
  };

  // cancel all changes
  $scope.cancel = function() {
    for (var i = $scope.jeffweiner.overview.length; i--;) {
      var row = $scope.jeffweiner.overview[i];    
      // undelete
      if (row.isDeleted) {
        delete row.isDeleted;
      }
      // remove new 
      if (row.isNew) {
        $scope.jeffweiner.overview.splice(i, 1);
      }      
    };
  };

  // save edits
  $scope.saveTable = function() {
    var results = [];
    for (var i = $scope.jeffweiner.overview.length; i--;) {
      var row = $scope.jeffweiner.overview[i];
      // actually delete row
      if (row.isDeleted) {
        $scope.jeffweiner.overview.splice(i, 1);
      }
      // mark as not new 
      if (row.isNew) {
        row.isNew = false;
      }

      // send on server
      // results.push($http.post('', row));      
    }

    return $q.all(results);
  };
});

linkedlnApp.controller('EditableTableCtrlSummary', function($scope, $filter, $http, $q) {
  $scope.checkName = function(data) {
    if (data ==='') {
      return "Can not be blank";
    }
  };

  // filter rows to show
  $scope.filterRow = function(row) {
    return row.isDeleted !== true;
  };

  // mark row as deleted
  $scope.deleteRow = function(id) {
    var filtered = $filter('filter')($scope.jeffweiner.summary, {'id': id});
    if (filtered.length) {
      filtered[0].isDeleted = true;
    }
  };

  // add row
  $scope.addRow = function() {
   $scope.jeffweiner.summary.push({
      'id': $scope.jeffweiner.summary.length+1,
      'content': '',
      isNew: true
    });
  };

  // cancel all changes
  $scope.cancel = function() {
    for (var i = $scope.jeffweiner.summary.length; i--;) {
      var row = $scope.jeffweiner.summary[i];    
      // undelete
      if (row.isDeleted) {
        delete row.isDeleted;
      }
      // remove new 
      if (row.isNew) {
        $scope.jeffweiner.summary.splice(i, 1);
      }      
    };
  };

  // save edits
  $scope.saveTable = function() {
    var results = [];
    for (var i = $scope.jeffweiner.summary.length; i--;) {
      var row = $scope.jeffweiner.summary[i];
      // actually delete row
      if (row.isDeleted) {
        $scope.jeffweiner.summary.splice(i, 1);
      }
      // mark as not new 
      if (row.isNew) {
        row.isNew = false;
      }

      // send on server
      // results.push($http.post('', row));      
    }

    return $q.all(results);
  };
});

linkedlnApp.controller('EditableTableCtrlProject', function($scope, $filter, $http, $q) {
  $scope.checkName = function(data) {
    if (data ==='') {
      return "Can not be blank";
    }
  };

  // filter rows to show
  $scope.filterRow = function(row) {
    return row.isDeleted !== true;
  };

  // mark row as deleted
  $scope.deleteRow = function(id) {
    var filtered = $filter('filter')($scope.jeffweiner.project, {'id': id});
    if (filtered.length) {
      filtered[0].isDeleted = true;
    }
  };

  // add row
  $scope.addRow = function() {
   $scope.jeffweiner.project.push({
      'id': $scope.jeffweiner.project.length+1,
      'name': '',
      'about': '',
      'member':'',
      isNew: true
    });
  };

  // cancel all changes
  $scope.cancel = function() {
    for (var i = $scope.jeffweiner.project.length; i--;) {
      var row = $scope.jeffweiner.project[i];    
      // undelete
      if (row.isDeleted) {
        delete row.isDeleted;
      }
      // remove new 
      if (row.isNew) {
        $scope.jeffweiner.project.splice(i, 1);
      }      
    };
  };

  // save edits
  $scope.saveTable = function() {
    var results = [];
    for (var i = $scope.jeffweiner.project.length; i--;) {
      var row = $scope.jeffweiner.project[i];
      // actually delete row
      if (row.isDeleted) {
        $scope.jeffweiner.project.splice(i, 1);
      }
      // mark as not new 
      if (row.isNew) {
        row.isNew = false;
      }

      // send on server
      // results.push($http.post('', row));      
    }

    return $q.all(results);
  };
});

linkedlnApp.controller('EditableTableCtrlExperience', function($scope, $filter, $http, $q) {
  $scope.checkName = function(data) {
    if (data ==='') {
      return "Can not be blank";
    }
  };

  // filter rows to show
  $scope.filterRow = function(row) {
    return row.isDeleted !== true;
  };

  // mark row as deleted
  $scope.deleteRow = function(id) {
    var filtered = $filter('filter')($scope.jeffweiner.experience, {'id': id});
    if (filtered.length) {
      filtered[0].isDeleted = true;
    }
  };

  // add row
  $scope.addRow = function() {
   $scope.jeffweiner.experience.push({
      'id': $scope.jeffweiner.experience.length+1,
      'job': '',
      'company': '',
      'time': '',
      isNew: true
    });
  };

  // cancel all changes
  $scope.cancel = function() {
    for (var i = $scope.jeffweiner.experience.length; i--;) {
      var row = $scope.jeffweiner.experience[i];    
      // undelete
      if (row.isDeleted) {
        delete row.isDeleted;
      }
      // remove new 
      if (row.isNew) {
        $scope.jeffweiner.experience.splice(i, 1);
      }      
    };
  };

  // save edits
  $scope.saveTable = function() {
    var results = [];
    for (var i = $scope.jeffweiner.experience.length; i--;) {
      var row = $scope.jeffweiner.experience[i];
      // actually delete row
      if (row.isDeleted) {
        $scope.jeffweiner.experience.splice(i, 1);
      }
      // mark as not new 
      if (row.isNew) {
        row.isNew = false;
      }

      // send on server
      // results.push($http.post('', row));      
    }

    return $q.all(results);
  };
});

linkedlnApp.controller('EditableTableCtrlEducation', function($scope, $filter, $http, $q) {
  $scope.checkName = function(data) {
    if (data ==='') {
      return "Can not be blank";
    }
  };

  // filter rows to show
  $scope.filterRow = function(row) {
    return row.isDeleted !== true;
  };

  // mark row as deleted
  $scope.deleteRow = function(id) {
    var filtered = $filter('filter')($scope.jeffweiner.education, {'id': id});
    if (filtered.length) {
      filtered[0].isDeleted = true;
    }
  };

  // add row
  $scope.addRow = function() {
   $scope.jeffweiner.education.push({
      'id': $scope.jeffweiner.education.length+1,
      'school': '',
      'major': '',
      'time': '',
      isNew: true
    });
  };

  // cancel all changes
  $scope.cancel = function() {
    for (var i = $scope.jeffweiner.education.length; i--;) {
      var row = $scope.jeffweiner.education[i];    
      // undelete
      if (row.isDeleted) {
        delete row.isDeleted;
      }
      // remove new 
      if (row.isNew) {
        $scope.jeffweiner.education.splice(i, 1);
      }      
    };
  };

  // save edits
  $scope.saveTable = function() {
    var results = [];
    for (var i = $scope.jeffweiner.education.length; i--;) {
      var row = $scope.jeffweiner.education[i];
      // actually delete row
      if (row.isDeleted) {
        $scope.jeffweiner.education.splice(i, 1);
      }
      // mark as not new 
      if (row.isNew) {
        row.isNew = false;
      }

      // send on server
      // results.push($http.post('', row));      
    }

    return $q.all(results);
  };
});


// ------------

linkedlnApp.controller('EditableCtrlSkill', function($scope) {
    $scope.addItem = function() {
        $scope.jeffweiner.skill.push({ 'id': $scope.jeffweiner.skill.length+1, 'value': 'New skill', isNew: true });
    };
    
    $scope.delete = function(value) {
         $scope.jeffweiner.skill.splice(value, 1);
  };
});